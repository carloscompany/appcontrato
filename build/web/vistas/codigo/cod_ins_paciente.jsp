<%-- 
    Document   : cod_ins_paciente
    Created on : 18-mar-2021, 14:33:01
    Author     : USUARIO
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="ReglasDeNegocio.sg_paciente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" href="../../css/sweetalert2.min.css"/>
<script type="text/javascript" src="../../js/sweetalert2.min.js"></script>
<%
    try {
        sg_paciente obj = new sg_paciente();
        sg_paciente.sg_pandemia obj_pan = obj.new sg_pandemia();
        obj.setNombre(request.getParameter("nombre"));
        obj.setApellido(request.getParameter("apellido"));
        obj.setDni(request.getParameter("dni"));
        try {
            String fecha_reg = request.getParameter("fecha_nac"); //1997-10-20
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
            Date fecha = dt.parse(fecha_reg);
            java.sql.Date fecha_nac = new java.sql.Date(fecha.getTime());
            obj.setFecha_nac(fecha_nac);
        } catch(ParseException e) {            
        }
        obj.setLat_dom(Double.parseDouble(request.getParameter("lat_dom")));
        obj.setLon_dom(Double.parseDouble(request.getParameter("lon_dom")));
        obj.setDireccion(request.getParameter("direccion"));
        obj.setNum_tel_con(request.getParameter("num_tel_con"));
        obj.setNum_tel_cel(request.getParameter("num_tel_cel"));
        obj.setE_mail(request.getParameter("email"));
        obj_pan.setNum_per_vive(Integer.parseInt(request.getParameter("num_per_vive")));
        obj_pan.setTrabaja(Boolean.parseBoolean(request.getParameter("trabaja")));
        obj_pan.setLugar_trabaja(request.getParameter("lugar_trabaja").equals("") ? 
                "NO APLICA" : request.getParameter("lugar_trabaja"));
        obj_pan.setEstudia(Boolean.parseBoolean(request.getParameter("estudia")));
        obj_pan.setLugar_estudia(request.getParameter("lugar_estudia").equals("") ?
                "NO APLICA" : request.getParameter("lugar_estudia"));
        obj_pan.setEnfer_catas(Boolean.parseBoolean(request.getParameter("enfer_catas")));
        obj_pan.setDiabetis(Boolean.parseBoolean(request.getParameter("diabetis")));
        obj_pan.setSobrepeso(Boolean.parseBoolean(request.getParameter("sobre_peso")));
        obj_pan.setIess_aseg(Boolean.parseBoolean(request.getParameter("iess_aseg")));
        obj_pan.setNom_contacEMER(request.getParameter("nom_contacEMER"));
        obj_pan.setNum_celEMER(request.getParameter("num_celEMER"));
        obj_pan.setEmailEMER(request.getParameter("e_mailEMER"));   
        obj.setPandemia(obj_pan);
        boolean result = sg_paciente.sg_insertar(obj);
        if (result) {
            out.println("<script> alert('Guardado Correctamente...'); location.replace('../../index.jsp');</script>");
        } else {
            out.println("<script> alert('No se ha guardado correctamente...'); location.replace('../../index.jsp');</script>");
        }
    } catch (Exception e) {
        out.println("<script> alert('No se ha guardado correctamente...'); location.replace('../../index.jsp');</script>");
    }

%>
