﻿-- Database: "AppWebPostGIS"

-- DROP DATABASE "AppWebPostGIS";

CREATE DATABASE "AppWebPostGIS"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Spanish_Spain.1252'
       LC_CTYPE = 'Spanish_Spain.1252'
       CONNECTION LIMIT = -1;

CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;

ALTER DATABASE "AppWebPostGIS"
  SET search_path = "$user", public, topology;

/*
DROP TABLE sg_paciente
DROP TABLE sg_pandemia
*/
CREATE TABLE sg_paciente
(
  paciente_id serial NOT NULL,
  nombre character varying(50) NOT NULL,
  apellido character varying(50) NOT NULL,
  dni character varying(10) NOT NULL,
  fecha_nac timestamp with time zone NOT NULL DEFAULT now(),
  direccion character varying(256) NOT NULL,
  ubicacion geometry NOT NULL,
  --lat_dom double precision NOT NULL,
  --lon_dom double precision NOT NULL,
  num_tel_con character varying(20) NOT NULL,
  num_tel_cel character varying(10) NOT NULL,
  e_mail character varying(256) NOT NULL DEFAULT 'SIN E_MAIL'::character varying,
  CONSTRAINT sg_paciente_pkey PRIMARY KEY (paciente_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sg_paciente
  OWNER TO postgres;

-- Function: sg_paciente_buscartodos()

-- DROP FUNCTION sg_paciente_buscartodos();
CREATE OR REPLACE FUNCTION sg_paciente_buscartodos(
    OUT ppaciente_id integer,
    OUT pnombre character varying,
    OUT papellido character varying,
    OUT pdni character varying,
    OUT pfecha_nac timestamp with time zone,
    OUT pdireccion character varying,
    OUT plat_dom double precision,
    OUT plon_dom double precision,
    OUT pnum_tel_con character varying(20),
    OUT pnum_tel_cel character varying(10),
    OUT pe_mail character varying(256),
    OUT pnum_per_vive integer,
    OUT ptrabaja boolean,
    OUT plugar_trabaja character varying,
    OUT pestudia boolean,
    OUT plugar_estudia character varying,
    OUT penfer_catas boolean,
    OUT pdiabetis boolean,
    OUT psobrepeso boolean,
    OUT piess_aseg boolean,
    OUT pnom_contacEMER character varying,
    OUT pnum_celEMER character varying,
    OUT pe_mailEMER character varying,
    OUT pcovid_19 boolean)
  RETURNS SETOF record AS
$BODY$begin
return query 
  SELECT pac.paciente_id,pac.nombre,pac.apellido,pac.dni,pac.fecha_nac,pac.direccion,ST_X(pac.ubicacion),ST_Y(pac.ubicacion),pac.num_tel_con,
  pac.num_tel_cel,pac.e_mail,pan.num_per_vive,pan.trabaja,pan.lugar_trabaja,pan.estudia,pan.lugar_estudia,pan.enfer_catas,pan.diabetis,pan.sobrepeso,pan.iess_aseg,
  pan.nom_contacEMER,pan.num_celEMER,pan.e_mailEMER,pan.covid_19
  FROM sg_paciente AS pac INNER JOIN sg_pandemia AS pan
  ON pac.paciente_id=pan.pandemia_id
  ORDER BY pac.paciente_id;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sg_paciente_buscartodos()
  OWNER TO postgres;

-- Function: sg_paciente_buscarporid(integer)

-- DROP FUNCTION sg_paciente_buscarporid(integer);

CREATE OR REPLACE FUNCTION sg_paciente_buscarporid(
    IN pipaciente_id integer,
    OUT ppaciente_id integer,
    OUT pnombre character varying,
    OUT papellido character varying,
    OUT pdni character varying,
    OUT pfecha_nac timestamp with time zone,
    OUT pdireccion character varying,
    OUT plat_dom double precision,
    OUT plon_dom double precision,
    OUT pnum_tel_con character varying(20),
    OUT pnum_tel_cel character varying(10),
    OUT pe_mail character varying(256),
    OUT pnum_per_vive integer,
    OUT ptrabaja boolean,
    OUT plugar_trabaja character varying,
    OUT pestudia boolean,
    OUT plugar_estudia character varying,
    OUT penfer_catas boolean,
    OUT pdiabetis boolean,
    OUT psobrepeso boolean,
    OUT piess_aseg boolean,
    OUT pnom_contacEMER character varying,
    OUT pnum_celEMER character varying,
    OUT pe_mailEMER character varying,
    OUT pcovid_19 boolean)
  RETURNS SETOF record AS
$BODY$begin
return query 
  SELECT pac.paciente_id,pac.nombre,pac.apellido,pac.dni,pac.fecha_nac,pac.direccion,ST_X(pac.ubicacion),ST_Y(pac.ubicacion),pac.num_tel_con,
  pac.num_tel_cel,pac.e_mail,pan.num_per_vive,pan.trabaja,pan.lugar_trabaja,pan.estudia,pan.lugar_estudia,pan.enfer_catas,pan.diabetis,pan.sobrepeso,pan.iess_aseg,
  pan.nom_contacEMER,pan.num_celEMER,pan.e_mailEMER,pan.covid_19
  FROM sg_paciente AS pac INNER JOIN sg_pandemia AS pan
  ON pac.paciente_id=pan.pandemia_id
  WHERE pac.paciente_id = $1 
  ORDER BY pac.paciente_id; 
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sg_paciente_buscarporid(integer)
  OWNER TO postgres;

-- Function: sg_paciente_insertar(character varying, character varying, character varying, timestamp with time zone, character varying, double precision, double precision, character varying, character varying, character varying)

-- DROP FUNCTION sg_paciente_insertar(character varying, character varying, character varying, timestamp with time zone, character varying, double precision, double precision, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sg_paciente_insertar(
    pinombre character varying,
    piapellido character varying,
    pidni character varying,
    pifecha_nac timestamp with time zone,
    pidireccion character varying,
    pilat_dom double precision,
    pilon_dom double precision,
    pinum_tel_con character varying,
    pinum_tel_cel character varying,
    pie_mail character varying)
  RETURNS boolean AS
$BODY$declare band boolean;
begin
    INSERT INTO sg_paciente(
            nombre, apellido,dni,fecha_nac,direccion,ubicacion,num_tel_con,num_tel_cel,e_mail)
    --VALUES ($1, $2, $3, $4,  ST_GeomFromText('POINT($5 $6)',4326), $7, $8, $9);
    VALUES ($1, $2, $3, $4, $5, ST_SetSRID(ST_MakePoint($6, $7),4326), $8, $9, $10);
	band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_paciente_insertar(character varying, character varying, character varying, timestamp with time zone, character varying, double precision, double precision, character varying, character varying, character varying)
  OWNER TO postgres;

-- Function: sg_paciente_editar(character varying, character varying, character varying, timestamp with time zone, character varying, double precision, double precision, character varying, character varying, character varying);

-- DROP FUNCTION sg_paciente_editar(character varying, character varying, character varying, timestamp with time zone, character varying, double precision, character varying, double precision, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION sg_paciente_editar(
    pipacienteid integer,
    pinombre character varying,
    piapellido character varying,
    pidni character varying,
    pifecha_nac timestamp with time zone,
    pidireccion character varying,
    pilat_dom double precision,
    pilon_dom double precision,
    pinum_tel_con character varying,
    pinum_tel_cel character varying,
    pie_mail character varying)
  RETURNS boolean AS
$BODY$declare band boolean;
begin	
    UPDATE sg_paciente
    SET nombre=$2, apellido=$3, dni=$4, fecha_nac=$5, direccion=$6, ubicacion= ST_SetSRID(ST_MakePoint($7, $8),4326), num_tel_con=$9, num_tel_cel=$10, e_mail=$11
    WHERE paciente_id=$1;
    band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_paciente_editar(integer,character varying, character varying, character varying, timestamp with time zone, character varying, double precision, double precision, character varying, character varying, character varying)
  OWNER TO postgres;

-- Function: sg_paciente_eliminar(integer)

-- DROP FUNCTION sg_paciente_eliminar(integer);

CREATE OR REPLACE FUNCTION sg_paciente_eliminar(pipaciente_id integer)
  RETURNS boolean AS
$BODY$declare band boolean;
begin
	DELETE FROM sg_paciente WHERE paciente_id=$1;
	band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_paciente_eliminar(integer)
  OWNER TO postgres;

-- Table: sg_pandemia

-- DROP TABLE sg_pandemia;

CREATE TABLE sg_pandemia
(
  pandemia_id serial NOT NULL,
  paciente_id integer NOT NULL,
  num_per_vive integer NOT NULL,
  trabaja boolean NOT NULL,
  lugar_trabaja character varying(50) DEFAULT 'NO APLICA' :: character varying, 
  estudia boolean NOT NULL,
  lugar_estudia character varying(50) DEFAULT 'NO APLICA' :: character varying,
  enfer_catas boolean NOT NULL,
  diabetis boolean NOT NULL,
  sobrepeso boolean NOT NULL,
  IESS_aseg boolean NOT NULL,
  nom_contacEMER character varying(50) NOT NULL,
  num_celEMER character varying(20) NOT NULL,
  e_mailEMER character varying(256) NOT NULL,
  covid_19 boolean DEFAULT false :: boolean,
  CONSTRAINT sg_pandemia_pkey PRIMARY KEY (pandemia_id),
  CONSTRAINT fk_paciente_id FOREIGN KEY (paciente_id)
      REFERENCES sg_paciente (paciente_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sg_pandemia
  OWNER TO postgres; 

-- Function: sg_pandemia_buscartodos()

-- DROP FUNCTION sg_pandemia_buscartodos();

CREATE OR REPLACE FUNCTION sg_pandemia_buscartodos(
    OUT ppandemia_id integer,
    OUT ppaciente_id integer,
    OUT pnum_per_vive integer,
    OUT ptrabaja boolean,
    OUT plugar_trabaja character varying,
    OUT pestudia boolean,
    OUT plugar_estudia character varying,
    OUT penfer_catas boolean,
    OUT pdiabetis boolean,
    OUT psobrepeso boolean,
    OUT piess_aseg boolean,
    OUT pnom_contacEMER character varying,
    OUT pnum_celEMER character varying,
    OUT pemailEMER character varying,
    OUT pcovid_19 boolean,
    OUT pnombre character varying,
    OUT papellido character varying,
    OUT pdni character varying,
    OUT pfecha_nac timestamp with time zone,
    OUT pdireccion character varying,
    OUT plat_dom double precision,
    OUT plon_dom double precision,
    OUT pnum_tel_con character varying(20),
    OUT pnum_tel_cel character varying(10),
    OUT pe_mail character varying(256))
  RETURNS SETOF record AS
$BODY$begin
return query 
  SELECT pan.pandemia_id,pan.paciente_id,pan.num_per_vive,pan.trabaja,pan.lugar_trabaja,pan.estudia,pan.lugar_estudia,pan.enfer_catas,pan.diabetis,pan.sobrepeso,pan.iess_aseg,pan.nom_contacEMER,pan.num_celEMER,
  pan.e_mailEMER,pan.covid_19,pac.paciente_id,pac.nombre,pac.apellido,pac.dni,pac.fecha_nac,pac.direccion,ST_X(pac.ubicacion),ST_Y(pac.ubicacion),pac.num_tel_con,
  pac.num_tel_cel,pac.e_mail
  FROM sg_pandemia AS pan INNER JOIN sg_paciente AS pac
  ON pan.paciente_id=pac.paciente_id
  ORDER BY pan.pandemia_id;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sg_pandemia_buscartodos()
  OWNER TO postgres;

-- Function: sg_pandemia_buscarporid(integer)

-- DROP FUNCTION sg_pandemia_buscarporid(integer);

CREATE OR REPLACE FUNCTION sg_pandemia_buscarporid(
    IN pipandemia_id integer,
    OUT ppaciente_id integer,
    OUT pnum_per_vive integer,
    OUT ptrabaja boolean,
    OUT plugar_trabaja character varying,
    OUT pestudia boolean,
    OUT plugar_estudia character varying,
    OUT penfer_catas boolean,
    OUT pdiabetis boolean,
    OUT psobrepeso boolean,
    OUT piess_aseg boolean,
    OUT pnom_contacEMER character varying,
    OUT pnum_celEMER character varying,
    OUT pemailEMER character varying,
    OUT pcovid_19 boolean,
    OUT pnombre character varying,
    OUT papellido character varying,
    OUT pdni character varying,
    OUT pfecha_nac timestamp with time zone,
    OUT pdireccion character varying,
    OUT plat_dom double precision,
    OUT plon_dom double precision,
    OUT pnum_tel_con character varying(20),
    OUT pnum_tel_cel character varying(10),
    OUT pe_mail character varying(256))
  RETURNS SETOF record AS
$BODY$begin
return query 
  SELECT pan.pandemia_id,pan.paciente_id,pan.num_per_vive,pan.trabaja,pan.lugar_trabaja,pan.estudia,pan.lugar_estudia,pan.enfer_catas,pan.diabetis,pan.sobrepeso,pan.iess_aseg,pan.nom_contacEMER,pan.num_celEMER,
  pan.e_mailEMER,pan.covid_19,pac.paciente_id,pac.nombre,pac.apellido,pac.dni,pac.fecha_nac,pac.direccion,ST_X(pac.ubicacion),ST_Y(pac.ubicacion),pac.num_tel_con,
  pac.num_tel_cel,pac.e_mail
  FROM sg_pandemia AS pan INNER JOIN sg_paciente AS pac
  ON pan.paciente_id=pac.paciente_id
  WHERE pan.pandemia_id = $1
  ORDER BY pan.pandemia_id; 
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sg_pandemia_buscarporid(integer)
  OWNER TO postgres;

-- Function: sg_pandemia_insertar(integer, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying, boolean)

-- DROP FUNCTION sg_pandemia_insertar(integer, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying, boolean);

CREATE OR REPLACE FUNCTION sg_pandemia_insertar(
    pipaciente_id integer,
    pinum_per_vive integer,
    pitrabaja boolean,
    pilugar_trabaja character varying,
    piestudia boolean,
    pilugar_estudia character varying,
    pienfer_catas boolean,
    pidiabetis boolean,
    pisobrepeso boolean,
    piiess_aseg boolean,
    pinum_contacEMER character varying,
    pinum_celEMER character varying,
    pie_mailEMER character varying,
    picovid_19 boolean)
  RETURNS boolean AS
$BODY$declare band boolean;
begin
    INSERT INTO sg_pandemia(
            paciente_id,num_per_vive,trabaja,lugar_trabaja,estudia,lugar_estudia,enfer_catas,diabetis,sobrepeso,iess_aseg,nom_contacEMER,num_celEMER,e_mailEMER, covid_19)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);
	band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_pandemia_insertar(integer, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying, boolean)
  OWNER TO postgres;

-- Function: sg_pandemia_editar(integer, integer, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying, boolean)

-- DROP FUNCTION sg_pandemia_editar(integer, integer, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying, boolean);

CREATE OR REPLACE FUNCTION sg_pandemia_editar(
    pipandemia_id integer,
    pipaciente_id integer,
    pinum_per_vive integer,
    pitrabaja boolean,
    pilugar_trabaja character varying,
    piestudia boolean,
    pilugar_estudia character varying,
    pienfer_catas boolean,
    pidiabetis boolean,
    pisobrepeso boolean,
    piiess_aseg boolean,
    pinom_contacEMER character varying,
    pinum_celEMER character varying,
    pie_mailEMER character varying,
    picovid_19 boolean)
  RETURNS boolean AS
$BODY$declare band boolean;
begin	
    UPDATE sg_pandemia
    SET paciente_id=$2, num_per_vive=$3, trabaja=$4, lugar_trabaja=$5, estudia=$6, lugar_estudia=$7, enfer_catas=$8, diabetis=$9, sobrepeso=$10, iess_aseg=$11, nom_contacEMER=$12, num_celEMER=$13, e_mailEMER=$14, covid_19=$15
    WHERE pandemia_id=$1;
    band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_pandemia_editar(integer, integer, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying, boolean)
  OWNER TO postgres;

-- Function: sg_pandemia_eliminar(integer)

-- DROP FUNCTION sg_pandemia_eliminar(integer);

CREATE OR REPLACE FUNCTION sg_pandemia_eliminar(pipandemia_id integer)
  RETURNS boolean AS
$BODY$declare band boolean;
begin
	DELETE FROM sg_pandemia WHERE pandemia_id=$1;
	band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_pandemia_eliminar(integer)
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION sg_insertar(
    pinombre character varying,
    piapellido character varying,
    pidni character varying,
    pifecha_nac timestamp with time zone,
    pidireccion character varying,
    pilat_dom double precision,
    pilon_dom double precision,
    pinum_tel_con character varying,
    pinum_tel_cel character varying,
    pie_mail character varying,
    pinum_per_vive integer,
    pitrabaja boolean,
    pilugar_trabaja character varying,
    piestudia boolean,
    pilugar_estudia character varying,
    pienfer_catas boolean,
    pidiabetis boolean,
    pisobrepeso boolean,
    piiess_aseg boolean,
    pinum_contacEMER character varying,
    pinum_celEMER character varying,
    pie_mailEMER character varying)
--RETURNS boolean AS
--$BODY$declare band boolean;
  RETURNS integer AS
$BODY$ declare ID integer;
begin
    INSERT INTO sg_paciente(
            nombre, apellido,dni,fecha_nac,direccion,ubicacion,num_tel_con,num_tel_cel,e_mail)
    VALUES ($1, $2, $3, $4, $5, ST_SetSRID(ST_MakePoint($6, $7),4326), $8, $9, $10);
	ID := (SELECT paciente_id FROM sg_paciente ORDER BY paciente_id DESC LIMIT 1);
    INSERT INTO sg_pandemia
	   (paciente_id,num_per_vive,trabaja,lugar_trabaja,estudia,lugar_estudia,enfer_catas,diabetis,sobrepeso,iess_aseg,nom_contacemer,num_celemer,e_mailemer,covid_19) 
    VALUES(ID,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,false);   
	--band=true;
	--commit;
	--return band;
	return ID;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_insertar(character varying, character varying, character varying, timestamp with time zone, character varying, double precision, double precision, character varying, character varying, character varying, integer, boolean, character varying, boolean, character varying, boolean, boolean, boolean, boolean, character varying, character varying, character varying)
  OWNER TO postgres;

--DROP FUNCTION sg_covid_19(integer);
CREATE OR REPLACE FUNCTION sg_covid_19(pipandemia_id integer)
  RETURNS boolean AS
$BODY$declare band boolean;
begin
	UPDATE sg_pandemia
	SET covid_19=true
	WHERE pandemia_id=$1;
	band=true;
	return band;
end;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION sg_covid_19(integer)
  OWNER TO postgres;
  
SELECT public.sg_insertar(
    'Carlos',
    'Condo',
    '0605017888',
    '1997-10-20',
    'Guano Sta. Teresita',
    -1.6642176,
    -78.6599761,
    '0998913310',
    '2930930',
    'calexiscondo@gmail.com',
    5,    
    false,
    'NO APLICA',
    true,
    'UNACH Universidad Nacional de Chimborazo',
    false,
    false,
    false,
    false,
    'Carlos Vicente Condo Machado',
    '0999984921',
    'ccarlosalexis@yahoo.com'
);
SELECT public.sg_covid_19(1);
SELECT *
FROM sg_paciente;
SELECT *
FROM sg_pandemia;