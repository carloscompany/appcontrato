/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReglasDeNegocio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author USUARIO
 */
public class Controlador {

    public static void SendMail(sg_paciente obj) {
        //System.out.println("Preparando para enviar el Correo Electronico");
        Properties properties = new Properties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");

        String userName = "calexiscondo@gmail.com";
        String password = "carloscondo22";
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password); //To change body of generated methods, choose Tools | Templates.
            }
        });
        MimeMessage message = prepareMessage(session, userName, obj.getE_mail() + " " + obj.getPandemia().getEmailEMER(), obj);
        try {
            //Transport.send(message);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", userName, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            //System.out.println("Mensaje Enviado Correctamente");
        } catch (MessagingException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static MimeMessage prepareMessage(Session session, String userName, String recipient, sg_paciente obj) {
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setHeader("Content-Type", "text/html");
            msg.setSubject("Mensaje HTML", "UTF-8");
            //msg.setText("`<html><body><h1 style =\"color:blue;\">My first Header<h1></body></html>`");
            //String html = "`<html><body><h1 style =\"color:blue;\">My first Header<h1></body></html>`";
            //String html = "`"+ leer() + "`";
            String html = leer(obj);
            //System.out.println(html);
            String[] emails = recipient.split(" ");
            InternetAddress dests[] = new InternetAddress[emails.length];
            for (int i = 0; i < emails.length; i++) {
                dests[i] = new InternetAddress(emails[i].trim().toLowerCase(),
                        emails[i].substring(0, emails[i].lastIndexOf("@")));
            }
            msg.setFrom(new InternetAddress(userName, "Administrador"));
            /*
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(recipient, "user"));
             */
            msg.addRecipients(Message.RecipientType.TO, dests);
            Multipart mp = new MimeMultipart();
            MimeBodyPart htmlPart = new MimeBodyPart();

            /*
            MimeBodyPart adjunto = new MimeBodyPart();
            DataSource source = new FileDataSource("C:\\12.jpg");
            adjunto.setDataHandler(new DataHandler(new FileDataSource("C:\\12.jpg")));
            adjunto.setDataHandler(new DataHandler(source));
            adjunto.setFileName("12.jpg");
             */
            MimeBodyPart file = new MimeBodyPart();
            DataSource source = new FileDataSource("web/files/10Things-spanish.pdf");
            file.setDataHandler(new DataHandler(new FileDataSource("web/files/10Things-spanish.pdf")));
            file.setDataHandler(new DataHandler(source));
            file.setFileName("10Things-spanish.pdf");
            //htmlPart.setHeader("Content-Type", "text/html");            
            htmlPart.setContent(html, "text/html;charset=utf-8");
            mp.addBodyPart(htmlPart);
            //mp.addBodyPart(adjunto);;
            mp.addBodyPart(file);
            msg.setContent(mp);
            msg.setSentDate(new java.util.Date());
            return msg;
        } catch (MessagingException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String leer(sg_paciente obj) {
        String texto = "", con = "";
        final String[] datos = {
            "NOMBRE_COMPLETO",
            "DNI",
            "FECHA_NAC",
            "DIRECCION",
            "TEL_CON",
            "TEL_CEL",
            "E_MAIL"
        };
        try {
            String[] palabras = null;
            String linea = "";
            int count = 0;
            File file = new File("web/files/covid19.html");
            String ruta = file.getAbsolutePath();
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            while ((texto = br.readLine()) != null) {
                count++;
                if (count == 144) { 
                    linea = texto.replace(datos[0], obj.getNombre() + " " + obj.getApellido());
                    linea = linea.replace(datos[1], obj.getDni());
                    linea = linea.replace(datos[2], obj.getFecha_nac().toString());
                    linea = linea.replace(datos[3], obj.getDireccion());
                    linea = linea.replace(datos[4], obj.getNum_tel_con());
                    linea = linea.replace(datos[5], obj.getNum_tel_cel());
                    linea = linea.replace(datos[6], obj.getE_mail());
                    con += linea;
                } else con += texto;
            }
            reader.close();
            return con;
        } catch (IOException e) {
            System.out.println("No se ha encontrado el Archivo" + e);
        }
        return "";
    }
    public static void main(String[] args) {
        try {
            sg_paciente pac = sg_paciente.sg_paciente_buscarporid(1);
            String ruta = leer(pac);
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}