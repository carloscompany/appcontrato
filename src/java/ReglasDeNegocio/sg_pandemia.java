/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReglasDeNegocio;

import AccesoDatos.Comando;
import AccesoDatos.Conexion;
import AccesoDatos.Global;
import AccesoDatos.Parametro;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author USUARIO
 */
public class sg_pandemia {
    private int pandemiaID;
    private int pacienteID;
    private int num_per_vive;
    private boolean trabaja;
    private String lugar_trabaja;
    private boolean estudia;
    private String lugar_estudia;
    private boolean enfer_catas;
    private boolean diabetis;
    private boolean sobrepeso;
    private boolean iess_aseg;
    private String nom_contacEMER;
    private String num_celEMER;
    private String emailEMER;

    public sg_pandemia() {
    }

    public sg_pandemia(int pandemiaID, int pacienteID, int num_per_vive, boolean trabaja, String lugar_trabaja, boolean estudia, String lugar_estudia, boolean enfer_catas, boolean diabetis, boolean sobrepeso, boolean iess_aseg, String nom_contacEMER, String num_celEMER, String emailEMER) {
        this.pandemiaID = pandemiaID;
        this.pacienteID = pacienteID;
        this.num_per_vive = num_per_vive;
        this.trabaja = trabaja;
        this.lugar_trabaja = lugar_trabaja;
        this.estudia = estudia;
        this.lugar_estudia = lugar_estudia;
        this.enfer_catas = enfer_catas;
        this.diabetis = diabetis;
        this.sobrepeso = sobrepeso;
        this.iess_aseg = iess_aseg;
        this.nom_contacEMER = nom_contacEMER;
        this.num_celEMER = num_celEMER;
        this.emailEMER = emailEMER;
    }

    public int getPandemiaID() {
        return pandemiaID;
    }

    public void setPandemiaID(int pandemiaID) {
        this.pandemiaID = pandemiaID;
    }

    public int getPacienteID() {
        return pacienteID;
    }

    public void setPacienteID(int pacienteID) {
        this.pacienteID = pacienteID;
    }

    public int getNum_per_vive() {
        return num_per_vive;
    }

    public void setNum_per_vive(int num_per_vive) {
        this.num_per_vive = num_per_vive;
    }

    public boolean isTrabaja() {
        return trabaja;
    }

    public void setTrabaja(boolean trabaja) {
        this.trabaja = trabaja;
    }

    public String getLugar_trabaja() {
        return lugar_trabaja;
    }

    public void setLugar_trabaja(String lugar_trabaja) {
        this.lugar_trabaja = lugar_trabaja;
    }

    public boolean isEstudia() {
        return estudia;
    }

    public void setEstudia(boolean estudia) {
        this.estudia = estudia;
    }

    public String getLugar_estudia() {
        return lugar_estudia;
    }

    public void setLugar_estudia(String lugar_estudia) {
        this.lugar_estudia = lugar_estudia;
    }

    public boolean isEnfer_catas() {
        return enfer_catas;
    }

    public void setEnfer_catas(boolean enfer_catas) {
        this.enfer_catas = enfer_catas;
    }

    public boolean isDiabetis() {
        return diabetis;
    }

    public void setDiabetis(boolean diabetis) {
        this.diabetis = diabetis;
    }

    public boolean isSobrepeso() {
        return sobrepeso;
    }

    public void setSobrepeso(boolean sobrepeso) {
        this.sobrepeso = sobrepeso;
    }

    public boolean isIess_aseg() {
        return iess_aseg;
    }

    public void setIess_aseg(boolean iess_aseg) {
        this.iess_aseg = iess_aseg;
    }

    public String getNom_contacEMER() {
        return nom_contacEMER;
    }

    public void setNom_contacEMER(String nom_contacEMER) {
        this.nom_contacEMER = nom_contacEMER;
    }

    public String getNum_celEMER() {
        return num_celEMER;
    }

    public void setNum_celEMER(String num_celEMER) {
        this.num_celEMER = num_celEMER;
    }

    public String getEmailEMER() {
        return emailEMER;
    }

    public void setEmailEMER(String emailEMER) {
        this.emailEMER = emailEMER;
    }
    
    
    
    public static ArrayList<sg_pandemia> sg_pandemia_buscartodos() throws Exception {
        ArrayList<sg_pandemia> lista = new ArrayList<>();
        sg_pandemia obj = new sg_pandemia();
        ResultSet rs = null;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        PreparedStatement pstm = null;
        try {
            String sql = "SELECT * FROM sg_pandemia_buscartodos()";
            pstm = con.creaPreparedSmt(sql);
            rs = con.ejecutaPrepared(pstm);
            obj = null;
            while(rs.next()) {
                obj = new sg_pandemia();
                obj.setPandemiaID(rs.getInt("ppandemia_id"));
                obj.setPacienteID(rs.getInt("ppaciente_id"));
                obj.setNum_per_vive(rs.getInt("pnum_per_vive"));
                obj.setTrabaja(rs.getBoolean("ptrabaja"));
                obj.setLugar_trabaja(rs.getString("plugar_trabaja"));
                obj.setEstudia(rs.getBoolean("pestudia"));
                obj.setLugar_trabaja(rs.getString("plugar_estudia"));
                obj.setEnfer_catas(rs.getBoolean("penfer_catas"));
                obj.setDiabetis(rs.getBoolean("pdiabetis"));
                obj.setSobrepeso(rs.getBoolean("psobrepeso"));
                obj.setIess_aseg(rs.getBoolean("piess_aseg"));
                obj.setNom_contacEMER(rs.getString("pnom_contacemer"));
                obj.setNum_celEMER(rs.getString("pnum_celmer"));
                obj.setEmailEMER(rs.getString("pemailemer"));
                lista.add(obj);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            rs.close();
            pstm.close();
            con.desconectar();
        }
        return lista;
    }
    public static sg_pandemia sg_pandemia_buscarporid(int ID_Pandemia) throws Exception{
        sg_pandemia obj = new sg_pandemia();
        ResultSet rs = null;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        PreparedStatement pstm = null;
        try {
            String sql = "SELECT * FROM sg_pandemia_buscarporid(?)";
            pstm = con.creaPreparedSmt(sql);
            pstm.setInt(1, ID_Pandemia);
            rs = con.ejecutaPrepared(pstm);
            obj = null;
            while(rs.next()) {
                obj = new sg_pandemia();
                obj.setPandemiaID(rs.getInt("ppandemia_id"));
                obj.setPacienteID(rs.getInt("ppaciente_id"));
                obj.setNum_per_vive(rs.getInt("pnum_per_vive"));
                obj.setTrabaja(rs.getBoolean("ptrabaja"));
                obj.setLugar_trabaja(rs.getString("plugar_trabaja"));
                obj.setEstudia(rs.getBoolean("pestudia"));
                obj.setLugar_estudia(rs.getString("plugar_estudia"));
                obj.setEnfer_catas(rs.getBoolean("penfer_catas"));
                obj.setDiabetis(rs.getBoolean("pdiabetis"));
                obj.setSobrepeso(rs.getBoolean("psobrepeso"));
                obj.setIess_aseg(rs.getBoolean("piess_aseg"));
                obj.setNom_contacEMER(rs.getString("pnum_contacemer"));
                obj.setNum_celEMER(rs.getString("pnum_celmer"));
                obj.setEmailEMER(rs.getString("pemailemer"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            rs.close();
            pstm.close();
            con.desconectar();
        }
        return obj;
    }
    public static boolean sg_pamdemia_insertar(sg_pandemia obj) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_pandemia_insertar(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();
            parametros.add(new Parametro(1, obj.getPacienteID()));
            parametros.add(new Parametro(2, obj.getNum_per_vive()));
            parametros.add(new Parametro(3, obj.isTrabaja()));
            parametros.add(new Parametro(4, obj.getLugar_trabaja()));
            parametros.add(new Parametro(5, obj.isEstudia()));
            parametros.add(new Parametro(6, obj.getLugar_estudia()));
            parametros.add(new Parametro(7, obj.isEnfer_catas()));
            parametros.add(new Parametro(8, obj.isDiabetis()));
            parametros.add(new Parametro(9, obj.isSobrepeso()));
            parametros.add(new Parametro(10, obj.isIess_aseg()));
            parametros.add(new Parametro(11, obj.getNom_contacEMER()));
            parametros.add(new Parametro(12, obj.getNum_celEMER()));
            parametros.add(new Parametro(13, obj.getEmailEMER()));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
    public static boolean sg_pandemia_editar(sg_pandemia obj) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_pandemia_editar(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();  
            parametros.add(new Parametro(1, obj.getPandemiaID()));
            parametros.add(new Parametro(2, obj.getPacienteID()));
            parametros.add(new Parametro(3, obj.getNum_per_vive()));
            parametros.add(new Parametro(4, obj.isTrabaja()));
            parametros.add(new Parametro(5, obj.getLugar_trabaja()));
            parametros.add(new Parametro(6, obj.isEstudia()));
            parametros.add(new Parametro(7, obj.getLugar_estudia()));
            parametros.add(new Parametro(8, obj.isEnfer_catas()));
            parametros.add(new Parametro(9, obj.isDiabetis()));
            parametros.add(new Parametro(10, obj.isSobrepeso()));
            parametros.add(new Parametro(11, obj.isIess_aseg()));
            parametros.add(new Parametro(12, obj.getNom_contacEMER()));
            parametros.add(new Parametro(13, obj.getNum_celEMER()));
            parametros.add(new Parametro(14, obj.getEmailEMER()));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
    public static boolean sg_pandemia_eliminar(int ID_Pandemia) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_pandemia_eliminar(?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();
            parametros.add(new Parametro(1, ID_Pandemia));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
}
