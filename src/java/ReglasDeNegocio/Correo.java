/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReglasDeNegocio;

/**
 *
 * @author USUARIO
 */
public class Correo {
    private String FROM;
    private String nombreArchivo;
    private String pass;
    private String fileName;
    private String TO;
    private String asunto;
    private String mensaje;

    public Correo() {
    }

    public Correo(String FROM, String nombreArchivo, String pass, String fileName, String TO, String asunto, String mensaje) {
        this.FROM = FROM;
        this.nombreArchivo = nombreArchivo;
        this.pass = pass;
        this.fileName = fileName;
        this.TO = TO;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    public String getFROM() {
        return FROM;
    }

    public void setFROM(String FROM) {
        this.FROM = FROM;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTO() {
        return TO;
    }

    public void setTO(String TO) {
        this.TO = TO;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    
}
