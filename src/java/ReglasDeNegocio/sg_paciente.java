/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReglasDeNegocio;
import AccesoDatos.Comando;
import AccesoDatos.Conexion;
import AccesoDatos.Global;
import AccesoDatos.Parametro;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author USUARIO
 */
public class sg_paciente {
    private int pacienteID;
    private String nombre;
    private String apellido;
    private String dni;
    private Date fecha_nac;
    private String direccion;
    private double lat_dom;
    private double lon_dom;
    private String num_tel_con;
    private String num_tel_cel;
    private String e_mail;
    private sg_pandemia pandemia;

    public sg_paciente() {
    }

    public sg_paciente(int pacienteID, String nombre, String apellido, String dni, Date fecha_nac, String direccion, double lat_dom, double lon_dom, String num_tel_con, String num_tel_cel, String e_mail, sg_pandemia pandemia) {
        this.pacienteID = pacienteID;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.fecha_nac = fecha_nac;
        this.direccion = direccion;
        this.lat_dom = lat_dom;
        this.lon_dom = lon_dom;
        this.num_tel_con = num_tel_con;
        this.num_tel_cel = num_tel_cel;
        this.e_mail = e_mail;
        this.pandemia = pandemia;
    }

    public int getPacienteID() {
        return pacienteID;
    }

    public void setPacienteID(int pacienteID) {
        this.pacienteID = pacienteID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Date getFecha_nac() {
        return fecha_nac;
    }

    public void setFecha_nac(Date fecha_nac) {
        this.fecha_nac = fecha_nac;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLat_dom() {
        return lat_dom;
    }

    public void setLat_dom(double lat_dom) {
        this.lat_dom = lat_dom;
    }

    public double getLon_dom() {
        return lon_dom;
    }

    public void setLon_dom(double lon_dom) {
        this.lon_dom = lon_dom;
    }

    public String getNum_tel_con() {
        return num_tel_con;
    }

    public void setNum_tel_con(String num_tel_con) {
        this.num_tel_con = num_tel_con;
    }

    public String getNum_tel_cel() {
        return num_tel_cel;
    }

    public void setNum_tel_cel(String num_tel_cel) {
        this.num_tel_cel = num_tel_cel;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public sg_pandemia getPandemia() {
        return pandemia;
    }

    public void setPandemia(sg_pandemia pandemia) {
        this.pandemia = pandemia;
    }
    
    public static boolean sg_insertar(sg_paciente obj) throws Exception {
        boolean respuesta = false;
        int ID=0;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_insertar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();
            parametros.add(new Parametro(1, obj.getNombre()));
            parametros.add(new Parametro(2, obj.getApellido()));
            parametros.add(new Parametro(3, obj.getDni()));
            parametros.add(new Parametro(4, obj.getFecha_nac()));
            parametros.add(new Parametro(5, obj.getDireccion()));
            parametros.add(new Parametro(6, obj.getLat_dom()));
            parametros.add(new Parametro(7, obj.getLon_dom()));
            parametros.add(new Parametro(8, obj.getNum_tel_con()));
            parametros.add(new Parametro(9, obj.getNum_tel_cel()));
            parametros.add(new Parametro(10, obj.getE_mail()));
            parametros.add(new Parametro(11, obj.getPandemia().getNum_per_vive()));
            parametros.add(new Parametro(12, obj.getPandemia().isTrabaja()));
            parametros.add(new Parametro(13, obj.getPandemia().lugar_trabaja));
            parametros.add(new Parametro(14, obj.getPandemia().isEstudia()));
            parametros.add(new Parametro(15, obj.getPandemia().lugar_estudia));
            parametros.add(new Parametro(16, obj.getPandemia().isEnfer_catas()));
            parametros.add(new Parametro(17, obj.getPandemia().isDiabetis()));
            parametros.add(new Parametro(18, obj.getPandemia().isSobrepeso()));
            parametros.add(new Parametro(19, obj.getPandemia().isIess_aseg()));
            parametros.add(new Parametro(20, obj.getPandemia().getNom_contacEMER()));
            parametros.add(new Parametro(21, obj.getPandemia().getNum_celEMER()));
            parametros.add(new Parametro(22, obj.getPandemia().getEmailEMER()));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            //respuesta = con.ejecutaPreparedTransaccion(comandos);
            ID = con.ejecutaTransaccion(comandos);
            if(ID > 0) respuesta = true;
            //int alea = (int)(Math.random() * 100);
            //if(alea > 50) { 
                sg_covid_19(ID);
                Controlador.SendMail(sg_paciente_buscarporid(ID));
            //}
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
    public static ArrayList<sg_paciente> sg_usuario_buscartodos() throws Exception {
        ArrayList<sg_paciente> lista = new ArrayList<>();
        sg_paciente obj = new sg_paciente();
        sg_paciente.sg_pandemia obj_pan = obj.new sg_pandemia();
        ResultSet rs = null;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        PreparedStatement pstm = null;
        try {
            String sql = "SELECT * FROM sg_paciente_buscartodos()";
            pstm = con.creaPreparedSmt(sql);
            rs = con.ejecutaPrepared(pstm);
            obj = null;
            while(rs.next()) {
                obj = new sg_paciente();
                obj_pan = obj.new sg_pandemia();
                obj.setPacienteID(rs.getInt("ppaciente_id"));
                obj.setNombre(rs.getString("pnombre"));
                obj.setApellido(rs.getString("papellido"));
                obj.setDni(rs.getString("pdni"));
                obj.setFecha_nac(rs.getDate("pfecha_nac"));
                obj.setDireccion(rs.getString("pdireccion"));
                obj.setLat_dom(rs.getDouble("plat_dom"));
                obj.setLon_dom(rs.getDouble("plon_dom"));
                obj.setNum_tel_con(rs.getString("pnum_tel_con"));
                obj.setNum_tel_cel(rs.getString("pnum_tel_cel"));
                obj.setE_mail(rs.getString("pe_mail"));
                obj_pan.setNum_per_vive(rs.getInt("pnum_per_vive"));
                obj_pan.setTrabaja(rs.getBoolean("ptrabaja"));
                obj_pan.setLugar_trabaja(rs.getString("plugar_trabaja"));
                obj_pan.setEstudia(rs.getBoolean("pestudia"));
                obj_pan.setLugar_estudia(rs.getString("plugar_estudia"));
                obj_pan.setEnfer_catas(rs.getBoolean("penfer_catas"));
                obj_pan.setDiabetis(rs.getBoolean("pdiabetis"));
                obj_pan.setSobrepeso(rs.getBoolean("psobrepeso"));
                obj_pan.setIess_aseg(rs.getBoolean("piess_aseg"));
                obj_pan.setNom_contacEMER(rs.getString("pnom_contacEMER"));
                obj_pan.setNum_celEMER(rs.getString("pnum_celEMER"));
                obj_pan.setEmailEMER(rs.getString("pe_mailEMER"));
                obj_pan.setCOVID_19(rs.getBoolean("pcovid_19"));
                obj.setPandemia(obj_pan);
                lista.add(obj);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            rs.close();
            pstm.close();
            con.desconectar();
        }
        return lista;
    }
    public static sg_paciente sg_paciente_buscarporid(int ID_Paciente) throws Exception{
        sg_paciente obj = new sg_paciente();
        sg_paciente.sg_pandemia obj_pan = obj.new sg_pandemia();
        ResultSet rs = null;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        PreparedStatement pstm = null;
        try {
            String sql = "SELECT * FROM sg_paciente_buscarporid(?)";
            pstm = con.creaPreparedSmt(sql);
            pstm.setInt(1, ID_Paciente);
            rs = con.ejecutaPrepared(pstm);
            obj = null;
            while(rs.next()) {
                obj = new sg_paciente();
                obj_pan = obj.new sg_pandemia();
                obj.setPacienteID(rs.getInt("ppaciente_id"));
                obj.setNombre(rs.getString("pnombre"));
                obj.setApellido(rs.getString("papellido"));
                obj.setDni(rs.getString("pdni"));
                obj.setFecha_nac(rs.getDate("pfecha_nac"));
                obj.setDireccion(rs.getString("pdireccion"));
                obj.setLat_dom(rs.getDouble("plat_dom"));
                obj.setLon_dom(rs.getDouble("plon_dom"));
                obj.setNum_tel_con(rs.getString("pnum_tel_con")); 
                obj.setNum_tel_cel(rs.getString("pnum_tel_cel")); 
                obj.setE_mail(rs.getString("pe_mail"));
                obj_pan.setIess_aseg(rs.getBoolean("piess_aseg"));
                obj_pan.setNum_per_vive(rs.getInt("pnum_per_vive"));
                obj_pan.setTrabaja(rs.getBoolean("ptrabaja"));
                obj_pan.setLugar_trabaja(rs.getString("plugar_trabaja"));
                obj_pan.setEstudia(rs.getBoolean("pestudia"));
                obj_pan.setLugar_estudia(rs.getString("plugar_estudia"));
                obj_pan.setEnfer_catas(rs.getBoolean("penfer_catas"));
                obj_pan.setDiabetis(rs.getBoolean("pdiabetis"));
                obj_pan.setSobrepeso(rs.getBoolean("psobrepeso"));
                obj_pan.setIess_aseg(rs.getBoolean("piess_aseg"));
                obj_pan.setNom_contacEMER(rs.getString("pnom_contacEMER"));
                obj_pan.setNum_celEMER(rs.getString("pnum_celEMER"));
                obj_pan.setEmailEMER(rs.getString("pe_mailEMER"));
                obj_pan.setCOVID_19(rs.getBoolean("pcovid_19"));
                obj.setPandemia(obj_pan);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            rs.close();
            pstm.close();
            con.desconectar();
        }
        return obj;
    }
    public static boolean sg_paciente_insertar(sg_paciente obj) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_insertar(?,?,?,?,?,?,?,?,?,?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();
            parametros.add(new Parametro(1, obj.getNombre()));
            parametros.add(new Parametro(2, obj.getApellido()));
            parametros.add(new Parametro(3, obj.getDni()));
            parametros.add(new Parametro(4, obj.getFecha_nac()));
            parametros.add(new Parametro(5, obj.getDireccion()));
            parametros.add(new Parametro(6, obj.getLat_dom()));
            parametros.add(new Parametro(7, obj.getLon_dom()));
            parametros.add(new Parametro(8, obj.getNum_tel_con()));
            parametros.add(new Parametro(9, obj.getNum_tel_cel()));
            parametros.add(new Parametro(10, obj.getE_mail()));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
    public static boolean sg_paciente_editar(sg_paciente obj) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_paciente_editar(?,?,?,?,?,?,?,?,?,?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();  
            parametros.add(new Parametro(1, obj.getPacienteID()));
            parametros.add(new Parametro(2, obj.getNombre()));
            parametros.add(new Parametro(3, obj.getApellido()));
            parametros.add(new Parametro(4, obj.getDni()));
            parametros.add(new Parametro(5, obj.getFecha_nac()));
            parametros.add(new Parametro(6, obj.getDireccion()));
            parametros.add(new Parametro(7, obj.getLat_dom()));
            parametros.add(new Parametro(8, obj.getLon_dom()));
            parametros.add(new Parametro(9, obj.getNum_tel_con()));
            parametros.add(new Parametro(10, obj.getNum_tel_cel()));
            parametros.add(new Parametro(11, obj.getE_mail()));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
    public static boolean sg_paciente_eliminar(int ID_Paciente) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_paciente_eliminar(?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();
            parametros.add(new Parametro(1, ID_Paciente));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
     public static boolean sg_covid_19(int ID_Pandemia) throws Exception {
        boolean respuesta = false;
        Conexion con =
                new Conexion(Global.driver, Global.url, Global.user, Global.pass);
        try {
            ArrayList<Comando> comandos = new ArrayList<Comando>();
            Comando cmd = new Comando();
            cmd.setSetenciaSql("SELECT * FROM sg_covid_19(?)");
            ArrayList<Parametro> parametros = new ArrayList<Parametro>();
            parametros.add(new Parametro(1, ID_Pandemia));
            cmd.setLstParametros(parametros);
            comandos.add(cmd);
            respuesta = con.ejecutaPreparedTransaccion(comandos);
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        } finally {
            con.desconectar();
        }
        return respuesta;
    }
    public class sg_pandemia {
        private int pandemiaID;
        private int pacienteID;
        private int num_per_vive;
        private boolean trabaja;
        private String lugar_trabaja;
        private boolean estudia;
        private String lugar_estudia;
        private boolean enfer_catas;
        private boolean diabetis;
        private boolean sobrepeso;
        private boolean iess_aseg;
        private String nom_contacEMER;
        private String num_celEMER;
        private String emailEMER;
        private boolean COVID_19;

        public sg_pandemia() {
        }

        public sg_pandemia(int pandemiaID, int pacienteID, int num_per_vive, boolean trabaja, String lugar_trabaja, boolean estudia, String lugar_estudia, boolean enfer_catas, boolean diabetis, boolean sobrepeso, boolean iess_aseg, String nom_contacEMER, String num_celEMER, String emailEMER, boolean COVID_19) {
            this.pandemiaID = pandemiaID;
            this.pacienteID = pacienteID;
            this.num_per_vive = num_per_vive;
            this.trabaja = trabaja;
            this.lugar_trabaja = lugar_trabaja;
            this.estudia = estudia;
            this.lugar_estudia = lugar_estudia;
            this.enfer_catas = enfer_catas;
            this.diabetis = diabetis;
            this.sobrepeso = sobrepeso;
            this.iess_aseg = iess_aseg;
            this.nom_contacEMER = nom_contacEMER;
            this.num_celEMER = num_celEMER;
            this.emailEMER = emailEMER;
            this.COVID_19 = COVID_19;
        }        

        public int getPandemiaID() {
            return pandemiaID;
        }

        public void setPandemiaID(int pandemiaID) {
            this.pandemiaID = pandemiaID;
        }

        public int getPacienteID() {
            return pacienteID;
        }

        public void setPacienteID(int pacienteID) {
            this.pacienteID = pacienteID;
        }

        public int getNum_per_vive() {
            return num_per_vive;
        }

        public void setNum_per_vive(int num_per_vive) {
            this.num_per_vive = num_per_vive;
        }

        public boolean isTrabaja() {
            return trabaja;
        }

        public void setTrabaja(boolean trabaja) {
            this.trabaja = trabaja;
        }

        public String getLugar_trabaja() {
            return lugar_trabaja;
        }

        public void setLugar_trabaja(String lugar_trabaja) {
            this.lugar_trabaja = lugar_trabaja;
        }

        public boolean isEstudia() {
            return estudia;
        }

        public void setEstudia(boolean estudia) {
            this.estudia = estudia;
        }

        public String getLugar_estudia() {
            return lugar_estudia;
        }

        public void setLugar_estudia(String lugar_estudia) {
            this.lugar_estudia = lugar_estudia;
        }

        public boolean isEnfer_catas() {
            return enfer_catas;
        }

        public void setEnfer_catas(boolean enfer_catas) {
            this.enfer_catas = enfer_catas;
        }

        public boolean isDiabetis() {
            return diabetis;
        }

        public void setDiabetis(boolean diabetis) {
            this.diabetis = diabetis;
        }

        public boolean isSobrepeso() {
            return sobrepeso;
        }

        public void setSobrepeso(boolean sobrepeso) {
            this.sobrepeso = sobrepeso;
        }

        public boolean isIess_aseg() {
            return iess_aseg;
        }

        public void setIess_aseg(boolean iess_aseg) {
            this.iess_aseg = iess_aseg;
        }

        public String getNom_contacEMER() {
            return nom_contacEMER;
        }

        public void setNom_contacEMER(String nom_contacEMER) {
            this.nom_contacEMER = nom_contacEMER;
        }

        public String getNum_celEMER() {
            return num_celEMER;
        }

        public void setNum_celEMER(String num_celEMER) {
            this.num_celEMER = num_celEMER;
        }

        public String getEmailEMER() {
            return emailEMER;
        }

        public void setEmailEMER(String emailEMER) {
            this.emailEMER = emailEMER;
        }

        public boolean isCOVID_19() {
            return COVID_19;
        }

        public void setCOVID_19(boolean COVID_19) {
            this.COVID_19 = COVID_19;
        }
        
        
    }
}