/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccesoDatos;

import java.util.ArrayList;

/**
 *
 * @author USUARIO
 */
public class Comando {

    private String setenciaSql;
    private ArrayList<Parametro> lstParametros;

    public Comando() {
        setenciaSql = "";
        lstParametros = new ArrayList<Parametro>();
    }

    /**
     * @return the setenciaSql
     */
    public String getSetenciaSql() {
        return setenciaSql;
    }

    /**
     * @param setenciaSql the setenciaSql to set
     */
    public void setSetenciaSql(String setenciaSql) {
        this.setenciaSql = setenciaSql;
    }

    /**
     * @return the lstParametros
     */
    public ArrayList<Parametro> getLstParametros() {
        return lstParametros;
    }

    /**
     * @param lstParametros the lstParametros to set
     */
    public void setLstParametros(ArrayList<Parametro> lstParametros) {
        this.lstParametros = lstParametros;
    }
}
