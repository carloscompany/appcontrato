/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$.validator.setDefaults({
    submitHandler: function () {
        //alert("submitted!");
    }
});
$(document).ready(function () {
    $('div.input-group input#lugar_estudia').hide();
    $('div.input-group input#lugar_trabaja').hide();
    $('input#estudia' ).on('click', function() {
        if( $(this).is(':checked') ){
            $(this).val('true');
            $('input#lugar_estudia').val("");
            $('input#lugar_estudia').fadeIn(2000);
        } else {
            $(this).val('false');
            $('input#lugar_estudia').val("");
            $('input#lugar_estudia').hide();
        }
    });
    $('input#trabaja' ).on('click', function() {
        if( $(this).is(':checked') ){
            $(this).val('true');
            $('input#lugar_trabaja').val("");
            $('input#lugar_trabaja').fadeIn(2000);
        } else {
            $(this).val('false');
            $('input#lugar_trabaja').val("");
            $('input#lugar_trabaja').hide();
        }
    });
    $("input:checkbox:checked").each(   
        function() {
            if($(this).is(':checked')) {
               $(this).val('true');
            }
        }
    );
    $("input:checkbox").each(   
        function() {
            if($(this).is(':checked')) {
               $(this).val('true');
            } else {
                $(this).val('false');
            }
        }
    );
    $.datetimepicker.setLocale('es');
    $('input[type=datetime]').datetimepicker({
        //format:'Y-m-d H:i',
        format: 'Y-m-d',
        inline:false,
        lang:'es'
    });
    $.validator.addMethod("digDIEZ", function (value, element) {
        var pattern = /^[0-9]{10}$/;
        return this.optional(element) || pattern.test(value);
    }, "El campo debe tener un valor de 10 caracteres");
    $.validator.addMethod("digSIETE", function (value, element) {
        var pattern = /^[0-9]{7}$/;
        return this.optional(element) || pattern.test(value);
    }, "El campo debe tener un valor de 7 caracteres");
    
    $("#myForm").validate({
        rules: {
            nombre: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            apellido: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            dni: {
                required: true,
                digDIEZ: true,
            },
            fecha_nac: "required",
            lugar_estudia: {
                minlength: 2,
                maxlength: 50
            },
            num_tel_con: {
                required: true,
                digSIETE: true
            },
            num_per_vive: "required",
            lugar_trabaja: {
                minlength: 2,
                maxlength: 50
            },
            nom_contacEMER: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            num_celEMER: {
                required: true,
                digDIEZ: true
            },
            e_mailEMER: {
                required: true,
                email: true
            },
            num_tel_cel: {
                required: true,
                digDIEZ: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            nombre: {
                required: "Por favor, ingreso su Nombre",
                minlength: "Minimo de 2 caracteres",
                maxlength: "Maximo de 50 caracteres"
                /*
                minlength: $.format("Mínimo de {0} caracteres."),
                maxlength: $.format("Máximo de {0} caracteres")
                */
            },
            apellido: { 
                required: "Por favor, ingreso su Apellido",
                minlength: "Minimo de 2 caracteres",
                maxlength: "Maximo de 50 caracteres"
                /*
                minlength: $.format("Mínimo de {0} caracteres"),
                maxlength: $.format("Máximo de (0) caracteres")
                */
            },
            fecha_nac: {
                required: "Seleccione la Fecha de Nacimiento"
            },
            lugar_estudia: {
                minlength: "Minimo de 2 caracteres",
                maxlength: "Maximo de 50 caracteres"
            },
            num_celEMER: {
                required: "Ingrese su número de Celular"
            },
            nom_contacEMER: {
                required: "Por favor, ingreso su Contacto de Emergencia",
                minlength: "Minimo de 2 caracteres",
                maxlength: "Maximo de 50 caracteres"
            },
            e_mailEMER: {
                required: "Por favor ingrese una direccion de correo",
                email: "Ingrese un correcta direccion de correcto example@.com"
            },
            dni: {
                required: "Por favor, ingrese DNI",
            },
            num_tel_con: {
                required: "Ingrese su número de Teléfono Convencional"
            },
            num_per_vive: {
                required: "Ingrese la cantidad de Personas que Viven"
            },
            num_tel_cel: {
                required: "Ingrese su número de Celular"
            },
            email: {
                required: "Por favor ingrese una direccion de correo",
                email: "Ingrese un correcta direccion de correcto example@.com"
            }
        },
        errorPlacement: function (error, element) {
            error.addClass("ui red pointing label transition");
            error.insertAfter(element.parent());
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".row").addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".row").removeClass(errorClass);
        },
        submitHandler: function () {
            //alert("submitted!");
            $("input:checkbox").each(   
                function() {
                    if($(this).is(':checked')) {
                       $(this).val('true');
                    } else {
                        $(this).val('false');
                    }
                }
            );
            $("#myForm").submit();
        }
    });
    if ($('#mapBox').length) {
        var $lat = $('#mapBox').data('lat');
        var $lon = $('#mapBox').data('lon');
        var $zoom = $('#mapBox').data('zoom');
        var $marker = $('#mapBox').data('marker');
        var $info = $('#mapBox').data('info');
        var $markerLat = $('#mapBox').data('mlat');
        var $markerLon = $('#mapBox').data('mlon');
        var map = new GMaps({
            el: '#mapBox',
            lat: $lat,
            lng: $lon,
            scrollwheel: false,
            scaleControl: true,
            streetViewControl: false,
            panControl: true,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            zoom: $zoom,
            styles: [
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#dcdfe6"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "stylers": [
                        {
                            "color": "#808080"
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#dcdfe6"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        },
                        {
                            "weight": 1.8
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#d7d7d7"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ebebeb"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#a7a7a7"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#efefef"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#696969"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#737373"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#d6d6d6"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {},
                {
                    "featureType": "poi",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#dadada"
                        }
                    ]
                }
            ]
        });
    }
    /*----------------------------------------------------*/
    /*  Google map js
    /*----------------------------------------------------*/
    if ($('#mapBox2').length) {
        var $lat = $('#mapBox2').data('lat');
        var $lon = $('#mapBox2').data('lon');
        var $zoom = $('#mapBox2').data('zoom');
        var $marker = $('#mapBox2').data('marker');
        var $info = $('#mapBox2').data('info');
        var $markerLat = $('#mapBox2').data('mlat');
        var $markerLon = $('#mapBox2').data('mlon');
        var map = new GMaps({
            el: '#mapBox2',
            lat: $lat,
            lng: $lon,
            scrollwheel: false,
            scaleControl: true,
            streetViewControl: false,
            panControl: true,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            zoom: $zoom,
            styles: [
                {
                    "featureType": "administrative.country",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                }
            ]
        });
    }

});