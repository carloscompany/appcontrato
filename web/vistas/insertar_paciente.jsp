<%-- 
    Document   : insertar_paciente
    Created on : 18-mar-2021, 14:23:37
    Author     : USUARIO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>Formulario COVID-19</title>
        <!-- Bootstrap CSS -->   
        <link rel="stylesheet" href="../css/semantic.css"/>
        <link rel="stylesheet" href="../css/bootstrap.css">   
        <link rel="stylesheet" href="../css/jquery.datetimepicker.css"/>
        <link rel="stylesheet" href="../css/jquery-ui.css"/>
        <link rel="stylesheet" href="../css/style.css"/>
        <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    </head>
    <body>
       <!--================ Start Registration Area =================-->
        <div class="section_gap registration_area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 offset-lg-1">
                        <div class="register_form">
                            <h3>Datos Personales del Paciente</h3>
                            <p>Ingrese sus datos personales en lo Solicitado</p>
                            <form class="form_area" id="myForm" action="codigo/cod_ins_paciente.jsp" method="post">
                                <div class="row">
                                    <div class="col-lg-6 form_group">
                                        <div class="ui input-group"><input id="nombre" name="nombre" placeholder="Nombre" type="text" /></div>
                                        <div class="ui input-group"><input id="apellido" name="apellido" placeholder="Apellido" type="text" /></div>
                                        <div class="ui input-group"><input id="dni" name="dni" placeholder="DNI" type="number" /></div>
                                        <div class="ui input-group"><input id="fecha_nac" name="fecha_nac" placeholder="Fecha Naciemiento" type="datetime" readonly="true"/></div>
                                        <ul id="mapaBuscar" class="form-group">
                                            <li><div class="ui input-group"><input class="search_latitude" id="lat_dom" name="lat_dom" placeholder="Latitud Domiciliaria" type="number" readonly="true"/></div></li>
                                            <li><div class="ui input-group"><input class="search_longitude" id="lon_dom" name="lon_dom" placeholder="Longitud Domiciliaria" type="number" readonly="true"/></div></li>
                                        </ul>
                                        <div class="ui input-group checklabel-pandemia">
                                            <input class="form-check-input" type="checkbox" value="" id="estudia" name="estudia">
                                            <label class="form-check-label" for="estudia">Estudia</label>
                                        </div>
                                        <div class="ui input-group"><input id="lugar_estudia" name="lugar_estudia" placeholder="Lugar Estudia" type="text" /></div>
                                        <div class="ui input-group checklabel-pandemia">
                                            <input class="form-check-input" type="checkbox" value="" id="enfer_catas" name="enfer_catas">
                                            <label class="form-check-label" for="enfer_catas">Enfermedad Catastrófica</label>
                                        </div>
                                        <div class="ui input-group checklabel-pandemia">
                                            <input class="form-check-input" type="checkbox" value="" id="diabetis" name="diabetis">
                                            <label class="form-check-label" for="diabetis">Diabetis</label>
                                        </div>
                                        <div class="ui input-group checklabel-pandemia">
                                            <input class="form-check-input" type="checkbox" value="" id="iess_aseg" name="iess_aseg">
                                            <label class="form-check-label" for="iess_aseg">IESS</label>
                                        </div>  
                                    </div>         
                                    <div class="col-lg-6 form-group">
                                        <div class="ui input-group"><input id="num_tel_con" name="num_tel_con" placeholder="Número Teléfono Convencional" type="number" /></div>
                                        <div class="ui input-group"><input id="num_tel_cel" name="num_tel_cel" placeholder="Número Celular" type="number" /></div>
                                        <div class="ui input-group"><input id="num_per_vive" name="num_per_vive" placeholder="Número Personas Vive" type="number" /></div>
                                        <div class="ui input-group checklabel-pandemia">
                                            <input class="form-check-input" type="checkbox" value="" id="trabaja" name="trabaja">
                                            <label class="form-check-label label-pandemia" for="trabaja">Trabaja</label>
                                        </div>
                                        <div class="ui input-group"><input id="lugar_trabaja" name="lugar_trabaja" placeholder="Lugar Trabaja" type="text" /></div>
                                        <div class="ui input-group"><input id="nom_contacEMER" name="nom_contacEMER" placeholder="Nombre Contacto Emergencia" type="text" /></div>
                                        <div class="ui input-group"><input id="num_celEMER" name="num_celEMER" placeholder="Numero Contacto Emergencia" type="number" /></div>
                                        <div class="ui input-group"><input id="e_mailEMER" name="e_mailEMER" placeholder="Email Contacto Emergencia" type="email" /></div>
                                        <div class="ui input-group checklabel-pandemia">
                                            <input class="form-check-input" type="checkbox" value="" id="sobre_peso" name="sobre_peso">
                                            <label class="form-check-label" for="sobre_peso">Sobrepeso</label>
                                        </div>
                                                                              
                                    </div>                                    
                                    <div class="col-lg-12 form-group">
                                        <div class="ui input-group"><input id="direccion" class="search_addr" name="direccion" placeholder="Dirección" type="text" readonly="true" /></div>
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <div class="ui input-group"><input id="email" name="email" placeholder="E mail" type="email" /></div>
                                    </div>                                    
                                    <div class="col-lg-12 form-group">
                                        <div class="form-group input-group">
                                            <input type="text" id="search_location" class="form-control" placeholder="Buscar Ubicación">
                                            <div class="input-group-btn">
                                                <button class="btn btn-secondary get_map" type="submit">
                                                    Localizar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="col-lg-12 text-center">
                                        <input type="submit" value="enviar" class="primary-btn" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
							
        <!--================ End Registration Area =================-->
        <h4>Mi Google Maps API</h4>
        <div id="mapBox" class="mapBox" data-lat="40.701083" data-lon="-74.1522848" data-zoom="13" data-info="PO Box CT16122 Collins Street West, Victoria 8007, Australia."
             data-mlat="40.701083" data-mlon="-74.1522848">
        </div>
        <!-- display google map -->
        <div id="geomap"></div>
        <!-- display selected location information -->
        <h4>Location Details</h4>
        <p>Address: <input type="text" class="search_addr" size="45" readonly="true"></p>
        <p>Latitude: <input type="text" class="search_latitude" readonly="true" size="30"></p>
        <p>Longitude: <input type="text" class="search_longitude" readonly="true" size="30"></p>
        <script src="../js/jquery-3.2.1.min.js"></script>
        <script src="../js/jquery.datetimepicker.full.js"></script>
        <script src="../js/jquery.validate.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-K2CBApH805IUeWU6tHi-rKIDHsTqDvU&callback=initMap&libraries=&v=weekly"></script>
        <script src="../js/gmaps.min.js"></script>
        <script src="../js/googleMaps.js"></script>
        <script src="../js/script.js"></script> 
    </body>
</html>
