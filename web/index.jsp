<%-- 
    Document   : index
    Created on : 15-mar-2021, 12:29:25
    Author     : USUARIO
--%>

<%@page import="ReglasDeNegocio.sg_paciente"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    /*AUTOS*/
    List<sg_paciente> lista = sg_paciente.sg_usuario_buscartodos();
    Iterator<sg_paciente> itTabla = lista.iterator();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>Formulario COVID-19</title>
        <!-- Bootstrap CSS -->   
        <link rel="stylesheet" href="css/semantic.css"/>
        <link rel="stylesheet" href="css/bootstrap.css">   
        <link rel="stylesheet" href="css/jquery.datetimepicker.css"/>
        <link rel="stylesheet" href="css/jquery-ui.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css"/>
    </head>
    <body>
        <div id="contenedor" class="container">
            <a href="vistas/insertar_paciente.jsp" class="genric-btn default-border circle">Añadir Paciente
                <span class="lnr lnr-arrow-right"></span>
            </a>
        </div>
        <h4>Mi Google Maps API</h4>        
        <div id="mapBox" class="mapBox" data-lat="40.701083" data-lon="-74.1522848" data-zoom="13" data-info="PO Box CT16122 Collins Street West, Victoria 8007, Australia."
             data-mlat="40.701083" data-mlon="-74.1522848">
        </div>
        <div class="table-responsive"> 
            <table id="tabla_Auto" class="table table-striped table-bordered display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center">Nombre Completo</th>
                        <th class="text-center">DNI</th>
                        <th class="text-center">Fecha Nacimiento</th>
                        <th class="text-center">Dirección</th>
                        <th class="text-center">Nº Celular</th>
                        <th class="text-center">E-mail</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <%while (itTabla.hasNext()) {
                        sg_paciente obj = itTabla.next();%>
                        <tr>
                            <td class="text-center"><%=obj.getNombre() + " " + obj.getApellido()%></td>
                            <td class="text-center"><%=obj.getDni()%></td>
                            <td class="text-center"><%=obj.getFecha_nac()%></td>
                            <td class="text-center"><%=obj.getDireccion()%></td>
                            <td class="text-center"><%=obj.getNum_tel_cel()%></td>
                            <td class="text-center"><%=obj.getE_mail()%></td>
                            <td class="text-center"><a class="btn btn-warning"  href="vistas/editar.jsp?codigo=<%=obj.getPacienteID()%>">Editar</a>
                                <a class="btn btn-danger" href="vistas/codigo/eliminar_auto.jsp?codigo=<%=obj.getPacienteID()%>"  onclick="return confirm('¿Está seguro que desea eliminar este registro?');">Eliminar</a></td>
                        </tr>
                    <%}%>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-center">Nombre Completo</th>
                        <th class="text-center">DNI</th>
                        <th class="text-center">Fecha Nacimiento</th>
                        <th class="text-center">Dirección</th>
                        <th class="text-center">Nº Celular</th>
                        <th class="text-center">E-mail</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/jquery.datetimepicker.full.js"></script>
        <script src="js/jquery.validate.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-K2CBApH805IUeWU6tHi-rKIDHsTqDvU&callback=initMap&libraries=&v=weekly"></script>
        <script src="js/gmaps.min.js"></script>
        <script src="js/googleMaps.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/dataTables.bootstrap4.min.js" type="text/javascript"></script>      
        <script type="text/javascript">
            $(document).ready(function() {
                $('#tabla_Auto').DataTable({
                    "language": {
                            "lengthMenu": "Mostrar _MENU_ registros",
                            "zeroRecords": "No se encontraron resultados",
                            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sSearch": "Buscar:",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "sProcessing": "Procesando...",
                        }
                });
            } );
        </script>
        <script src="js/script.js"></script> 
    </body>
</html>
